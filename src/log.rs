use crate::{ComputeNode, Result};

pub struct Log<C: ComputeNode> {
    argument: C,
    base: f32,
}

impl<C: ComputeNode> Log<C> {
    pub fn new(argument: C, base: f32) -> Self {
        Self { argument, base }
    }
}

impl<C: ComputeNode> ComputeNode for Log<C> {
    fn compute(&self) -> Result<f32> {
        Ok(f32::log(self.argument.compute()?, self.base))
    }
}

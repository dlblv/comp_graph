use crate::{ComputeNode, Result};

pub struct Sin<C: ComputeNode> {
    argument: C,
}

impl<C: ComputeNode> Sin<C> {
    pub fn new(argument: C) -> Self {
        Self { argument }
    }
}

impl<C: ComputeNode> ComputeNode for Sin<C> {
    fn compute(&self) -> Result<f32> {
        Ok(f32::sin(self.argument.compute()?))
    }
}

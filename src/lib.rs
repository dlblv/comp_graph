mod error;
pub mod input;
pub mod log;
pub mod ops;
pub mod sin;

#[cfg(test)]
mod tests;

pub use error::{Error, Result};

pub trait ComputeNode {
    fn compute(&self) -> Result<f32>;
}

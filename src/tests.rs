use crate::{
    input::Input,
    log::Log,
    ops::{Add, Div, Mul, Sub},
    sin::Sin,
    ComputeNode, Error,
};

fn round(x: f32, precision: u32) -> f32 {
    let m = 10i32.pow(precision) as f32;
    (x * m).round() / m
}

#[test]
fn inputs() {
    let input = Input::new();
    assert_eq!(input.compute(), Err(Error::EmptyInput));
    input.set(0.);
    assert_eq!(input.compute(), Ok(0.));
    input.set(11.);
    assert_eq!(input.compute(), Ok(11.));

    let input2 = Input::from(12.);
    assert_eq!(input2.compute(), Ok(12.));
}

#[test]
fn sin() {
    let input = Input::from(0.);
    let sin = Sin::new(&input);
    assert_eq!(sin.compute(), Ok(0.));
    input.set(30.);
    assert_eq!(sin.compute().map(|x| round(x, 5)), Ok(-0.98803));
    input.set(std::f32::consts::PI);
    assert_eq!(sin.compute().map(|x| round(x, 5)), Ok(-0.));
    input.set(std::f32::consts::PI / 2.0);
    assert_eq!(sin.compute().map(|x| round(x, 5)), Ok(1.));

    input.set(0.);
    let sinsin = Sin::new(sin);
    assert_eq!(sinsin.compute(), Ok(0.));
    input.set(1.);
    assert_eq!(sinsin.compute().map(|x| round(x, 5)), Ok(0.74562));
}

#[test]
fn log() {
    let input = Input::from(100.);
    let log = Log::new(&input, 10.);
    assert_eq!(log.compute().map(|x| round(x, 5)), Ok(2.));
}

#[test]
fn ops() {
    let a = Input::from(6.);
    let b = Input::from(2.);

    assert_eq!(Add::new(&a, &b).compute(), Ok(8.));
    assert_eq!(Sub::new(&a, &b).compute(), Ok(4.));
    assert_eq!(Mul::new(&a, &b).compute(), Ok(12.));
    assert_eq!(Div::new(&a, &b).compute(), Ok(3.));
}

#[test]
fn it_works() {
    // x1, x2, x3 implement trait ComputeNode
    let x1: Input = Input::new();
    let x2 = Input::new();
    let x3 = Input::new();

    // sin and log are structs which implement the trait ComputeNode.
    // + and * are opps which take two ComputeNodes and produce a single ComputeNode.
    let output = Add::new(
        &x1,
        Mul::new(&x2, Sin::new(Add::new(&x2, Log::new(&x3, 2.)))),
    );

    x1.set(1f32);
    x2.set(2f32);
    x3.set(3f32);
    let result = output.compute().expect("Compute failed");
    println!("Graph output = {}", result);
    assert_eq!(round(result, 5), 0.14203);

    x1.set(2f32);
    x2.set(3f32);
    x3.set(4f32);
    let result = output.compute().expect("Compute failed");
    println!("Graph output = {}", result);
    assert_eq!(round(result, 5), -0.87677);
}

use crate::{ComputeNode, Result};
pub struct Add<L: ComputeNode, R: ComputeNode> {
    lhs: L,
    rhs: R,
}

impl<L: ComputeNode, R: ComputeNode> Add<L, R> {
    pub fn new(lhs: L, rhs: R) -> Self {
        Self { lhs, rhs }
    }
}

impl<L: ComputeNode, R: ComputeNode> ComputeNode for Add<L, R> {
    fn compute(&self) -> Result<f32> {
        Ok(self.lhs.compute()? + self.rhs.compute()?)
    }
}

pub struct Sub<L: ComputeNode, R: ComputeNode> {
    lhs: L,
    rhs: R,
}

impl<L: ComputeNode, R: ComputeNode> Sub<L, R> {
    pub fn new(lhs: L, rhs: R) -> Self {
        Self { lhs, rhs }
    }
}

impl<L: ComputeNode, R: ComputeNode> ComputeNode for Sub<L, R> {
    fn compute(&self) -> Result<f32> {
        Ok(self.lhs.compute()? - self.rhs.compute()?)
    }
}

pub struct Mul<L: ComputeNode, R: ComputeNode> {
    lhs: L,
    rhs: R,
}

impl<L: ComputeNode, R: ComputeNode> Mul<L, R> {
    pub fn new(lhs: L, rhs: R) -> Self {
        Self { lhs, rhs }
    }
}

impl<L: ComputeNode, R: ComputeNode> ComputeNode for Mul<L, R> {
    fn compute(&self) -> Result<f32> {
        Ok(self.lhs.compute()? * self.rhs.compute()?)
    }
}

pub struct Div<L: ComputeNode, R: ComputeNode> {
    lhs: L,
    rhs: R,
}

impl<L: ComputeNode, R: ComputeNode> Div<L, R> {
    pub fn new(lhs: L, rhs: R) -> Self {
        Self { lhs, rhs }
    }
}

impl<L: ComputeNode, R: ComputeNode> ComputeNode for Div<L, R> {
    fn compute(&self) -> Result<f32> {
        Ok(self.lhs.compute()? / self.rhs.compute()?)
    }
}

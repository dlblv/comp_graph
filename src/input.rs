use crate::{ComputeNode, Error, Result};
use std::cell::Cell;

pub struct Input(Cell<Option<f32>>);

impl Input {
    pub fn new() -> Self {
        Self(Cell::new(None))
    }

    pub fn set(&self, value: f32) {
        self.0.set(Some(value));
    }
}

impl ComputeNode for Input {
    fn compute(&self) -> Result<f32> {
        self.0.get().ok_or(Error::EmptyInput)
    }
}

impl ComputeNode for &Input {
    fn compute(&self) -> Result<f32> {
        self.0.get().ok_or(Error::EmptyInput)
    }
}

impl From<f32> for Input {
    fn from(value: f32) -> Self {
        Self(Cell::new(Some(value)))
    }
}

impl Default for Input {
    fn default() -> Self {
        Self::new()
    }
}

impl Clone for Input {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

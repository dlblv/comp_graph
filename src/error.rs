use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
pub enum Error {
    #[error("empty input: set input value before using")]
    EmptyInput,
}

pub type Result<T> = ::std::result::Result<T, Error>;
